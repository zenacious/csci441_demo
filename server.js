const app = require('./js/database/db_app').app;
const db = require('./js/database/db_conn');
require('./js/database/api/administrators');
require('./js/database/api/employees');
require('./js/database/api/inventory');
require('./js/database/api/orders');


//
// Start Node Server
//
db.connect((err) => {
    if (err){
        console.log('unable to connect to database');
        process.exit(1);
    }
    else {
        app.listen(5000, () => {
            console.log('connected to database, app listening on port 5000');
        });
    }
});