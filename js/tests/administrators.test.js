const { MongoClient } = require('mongodb');
const fetch = require('node-fetch');


describe('administrators collection', () => {
    let connection;
    let db;
    let administrators;
    const url = 'http://localhost:5000/administrators';
    const mockAdministrator = {
        name: 'Jack Smith', username: 'jsmith', password: 'p@ssw)rd'
    };

    beforeAll(async () => {
        connection = await MongoClient.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = await connection.db('csci441');
        administrators = await db.collection('administrators');
    });

    afterAll(async () => {
        let res;
        do {
            res = await administrators.findOneAndDelete({ name: 'Jack Smith' });
        } while (res.ok === 1);
        await connection.close();
        await db.close();
    });

    it ('should insert new administrator document into collection', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockAdministrator),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => {
                mockAdministrator._id = json.result.insertedId;
                expect(json.result.ok).toBe(1);
            });
    });

    it ('should not insert administrator document with duplicate username', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockAdministrator),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => expect(json.keyValue).toEqual({ username: mockAdministrator.username }));
    });

    it ('should retrieve a single administrator document', async () => {
        const mockUrl = url + '/' + mockAdministrator._id;
        await fetch(mockUrl, { method: 'GET' })
            .then(res => res.json())
            .then(json => json[0])
            .then(doc => {
                expect(doc.name).toEqual(mockAdministrator.name);
                expect(doc.username).toEqual(mockAdministrator.username);
                expect(doc._id).toEqual(mockAdministrator._id);
            });
    });

    it ('should not store administrator document password in plain text', async () => {
        const mockUrl = url + '/' + mockAdministrator._id;
        await fetch(mockUrl, { method: 'GET' })
            .then(res => res.json())
            .then(json => json[0])
            .then(doc => {
                expect(doc.password).not.toEqual(mockAdministrator.password);
            });
    });

    it ('should log in with correct username and password', async () => {
        const mockUrl = url + '/login';
        const options = {
            method: 'POST',
            body: JSON.stringify({ username: mockAdministrator.username, password: mockAdministrator.password }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => expect(res.status).toBe(200));
    });

    it ('should fail to log in with correct username and incorrect password', async () => {
        const mockUrl = url + '/login';
        const options = {
            method: 'POST',
            body: JSON.stringify({ username: mockAdministrator.username, password: 'incorrect_password' }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => expect(res.status).toBe(401));
    });

    it ('should fail to log in with incorrect username and correct password', async () => {
        const mockUrl = url + '/login';
        const options = {
            method: 'POST',
            body: JSON.stringify({ username: 'mjackson', password: mockAdministrator.password }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => expect(res.status).toBe(400));
    });

    it ('should fail to log in with incorrect username and password', async () => {
        const mockUrl = url + '/login';
        const options = {
            method: 'POST',
            body: JSON.stringify({ username: 'mjackson', password: 'incorrect_password' }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => expect(res.status).toBe(400));
    });

    it ('should retrieve all administrator documents', async () => {
        const mockAdministrators = [
            { name: 'Jack Smith', username: 'jsmith2', password: 'p@ssw)rd' },
            { name: 'Jack Smith', username: 'jsmith3', password: 'p@ssw)rd' },
            { name: 'Jack Smith', username: 'jsmith4', password: 'p@ssw)rd' },
        ];
        await administrators.insertMany(mockAdministrators);
        await fetch(url, { method: 'GET' })
            .then(res => res.json())
            .then(json => expect(json.length).toBeGreaterThanOrEqual(3));
    });

    it ('should update a field of an administrator document', async () => {
        const adminUpdate = { name: 'Jack Smith', username: 'sjack', password: 'user1234'};
        const mockUrl = url + '/' + mockAdministrator._id;
        const options = {
            method: 'PUT',
            body: JSON.stringify(adminUpdate),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => res.json())
            .then(async json => {
                expect(json.result.ok).toBe(1);
                expect(json.document.name).toEqual(adminUpdate.name);
                expect(json.document.username).toEqual(adminUpdate.username);
                // check password updated by validating authentication
                const mockUrl = url + '/login';
                const options = {
                    method: 'POST',
                    body: JSON.stringify({ username: adminUpdate.username, password: adminUpdate.password }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                await fetch(mockUrl, options)
                    .then(res => expect(res.status).toBe(200));
            });

    });

    it ('should delete an administrator document', async () => {
        const mockUrl = url + '/' + mockAdministrator._id;
        await fetch(mockUrl, { method: 'DELETE' })
            .then(res => res.json())
            .then(json => expect(json.ok).toBe(1));
    });
});