const { hello } = require('../database/api/hello');

test('should be defined', () => {
   expect(hello).toBeDefined();
});

test('given kevin as name', () => {
   expect(hello('kevin')).toBe('Hello kevin');
});