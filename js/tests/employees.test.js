const { MongoClient } = require('mongodb');
const fetch = require('node-fetch');

describe('employees collection', () => {
    let connection;
    let db;
    let employees;
    const url = 'http://localhost:5000/employees';
    const mockEmployee = {
        name: 'John Smith', position: 'Server', rate: 10.55
    };

    beforeAll(async () => {
        connection = await MongoClient.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = await connection.db('csci441');
        employees = await db.collection('employees');
    });

    afterAll(async () => {
        let res;
        do {
            res = await employees.findOneAndDelete({name: 'John Smith'});
        } while (res.ok === 1);
        await connection.close();
        await db.close();
    });

    it('should insert new employee document into collection', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockEmployee),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => {
                mockEmployee._id = json.result.insertedId;
                expect(json.result.ok).toBe(1);
            });
    });

    it('should retrieve a single employee document', async () => {
        const mockUrl = url + '/' + mockEmployee._id;
        await fetch(mockUrl, { method: 'GET' })
            .then(res => res.json())
            .then(json => json[0])
            .then(doc => {
                expect(doc.name).toEqual(mockEmployee.name);
                expect(doc.position).toEqual(mockEmployee.position);
                expect(doc.rate).toEqual(mockEmployee.rate);
            });
    });

    it('should retrieve all employee documents', async () => {
        const mockEmployees = [
            { name: 'John Smith', position: 'Server', rate: 10.55 },
            { name: 'John Smith', position: 'Server', rate: 10.55 },
            { name: 'John Smith', position: 'Server', rate: 10.55 },
        ];
        await employees.insertMany(mockEmployees);
        await fetch(url, { method: 'GET' })
            .then(res => res.json())
            .then(json => expect(json.length).toBeGreaterThanOrEqual(3));
    });

    it ('should update a field of an employee document', async () => {
        const employeeUpdate = { name: 'John Smith', position: 'Assistant Manager', rate: 18.55 };
        const mockUrl = url + '/' + mockEmployee._id;
        const options = {
            method: 'PUT',
            body: JSON.stringify(employeeUpdate),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => res.json())
            .then(json => {
                expect(json.result.ok).toBe(1);
                expect(json.document.name).toEqual(employeeUpdate.name);
                expect(json.document.position).toEqual(employeeUpdate.position);
                expect(json.document.rate).toEqual(employeeUpdate.rate);
            });
    });

    it ('should delete an employee document', async () => {
        const mockUrl = url + '/' + mockEmployee._id;
        await fetch(mockUrl, { method: 'DELETE' })
            .then(res => res.json())
            .then(json => expect(json.ok).toBe(1));
    });
});