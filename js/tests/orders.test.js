const { MongoClient } = require('mongodb');
const fetch = require('node-fetch');

describe('orders collection', () => {
    let connection;
    let db;
    let orders;
    const url = 'http://localhost:5000/orders';
    const mockOrder = {
        server: '5de1b7927a1ec019c8594905',
        table: 11,
        order: [
            {
                _id: '5de191a2d23d4b151816037f',
                qty: 1,
            },
            {
                _id: '5de2f99772e01e25c0a61705',
                qty: 3,
            }
        ]
    };

    beforeAll(async () => {
        connection = await MongoClient.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = await connection.db('csci441');
        orders = await db.collection('orders');
    });

    afterAll(async () => {
        let res;
        do {
            res = await orders.findOneAndDelete( { table: 11 });
        } while (res.ok === 1);
        await connection.close();
        await db.close();
    });

    it ('should insert new order document into collection', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockOrder),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => {
                mockOrder._id = json.result.insertedId;
                expect(json.result.ok).toBe(1);
            });
    });

    it ('should retrieve a single order document', async () => {
        const mockUrl = url + '/' + mockOrder._id;
        await fetch(mockUrl, { method: 'GET' })
            .then(res => res.json())
            .then(json => json[0])
            .then(doc => {
                expect(doc.server).toEqual(mockOrder.server);
                expect(doc.table).toEqual(mockOrder.table);
                expect(doc.order).toEqual(mockOrder.order);
            });
    });

    it ('should retrieve all order documents', async () => {
        const mockOrders = [
            {server:'5de1b7927a1ec019c8594905',table:11,order:[{_id:'5de191a2d23d4b151816037f',qty:1},{_id:'5de2f99772e01e25c0a61705',qty:3}]},
            {server:'5de1b7927a1ec019c8594905',table:11,order:[{_id:'5de191a2d23d4b151816037f',qty:1},{_id:'5de2f99772e01e25c0a61705',qty:3}]},
            {server:'5de1b7927a1ec019c8594905',table:11,order:[{_id:'5de191a2d23d4b151816037f',qty:1},{_id:'5de2f99772e01e25c0a61705',qty:3}]},
        ];
        await orders.insertMany(mockOrders);
        await fetch(url, { method: 'GET' })
            .then(res => res.json())
            .then(json => expect(json.length).toBeGreaterThanOrEqual(3));
    });

    it ('should update a field of an order document', async () => {
        const orderUpdate = {server:'5de1b7927a1ec019c8594905',table:11,order:[{_id:'5de191a2d23d4b151816037f',qty:2},{_id:'5de2f99772e01e25c0a61705',qty:2}]};
        const mockUrl = url + '/' + mockOrder._id;
        const options = {
            method: 'PUT',
            body: JSON.stringify(orderUpdate),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => res.json())
            .then(json => {
                expect(json.result.ok).toBe(1);
                expect(json.document.server).toEqual(orderUpdate.server);
                expect(json.document.table).toEqual(orderUpdate.table);
                expect(json.document.order).toEqual(orderUpdate.order);
            });
    });

    it ('should delete an order document', async () => {
        const mockUrl = url + '/' + mockOrder._id;
        await fetch(mockUrl, { method: 'DELETE' })
            .then(res => res.json())
            .then(json => expect(json.ok).toBe(1));
    });
});