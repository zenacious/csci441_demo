const { MongoClient } = require('mongodb');
const fetch = require('node-fetch');

describe('inventory collection', () => {
    let connection;
    let db;
    let inventory;
    const url = 'http://localhost:5000/inventory';
    const mockInventory = {
        menu_number: 11, name: 'Tasty Cakes', price: 4.59, qty: 500
    };

    beforeAll(async () => {
        connection = await MongoClient.connect('mongodb://localhost:27017', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        db = await connection.db('csci441');
        inventory = await db.collection('inventory');
    });

    afterAll(async () => {
        let res;
        do {
            res = await inventory.findOneAndDelete({ name: 'Tasty Cakes' });
        } while (res.ok === 1);
        await connection.close();
        await db.close();
    });

    it ('should insert new inventory document into collection', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockInventory),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => {
                mockInventory._id = json.result.insertedId;
                expect(json.result.ok).toBe(1);
            });
    });

    it ('should not insert inventory document with duplicate menu_number', async () => {
        const options = {
            method: 'POST',
            body: JSON.stringify(mockInventory),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, options)
            .then(res => res.json())
            .then(json => expect(json.keyValue).toEqual({ menu_number: mockInventory.menu_number }));
    });

    it ('should retrieve a single inventory document', async () => {
        const mockUrl = url + '/' + mockInventory._id;
        await fetch(mockUrl, { method: 'GET' })
            .then(res => res.json())
            .then(json => json[0])
            .then(doc => {
                expect(doc.name).toEqual(mockInventory.name);
                expect(doc.menu_number).toEqual(mockInventory.menu_number);
                expect(doc.price).toEqual(mockInventory.price);
                expect(doc.qty).toEqual(mockInventory.qty);
            });
    });

    it ('should retrieve all inventory documents', async () => {
        const mockInvetories = [
            { menu_number: 12, name: 'Tasty Cakes', price: 4.59, qty: 500 },
            { menu_number: 13, name: 'Tasty Cakes', price: 4.59, qty: 500 },
            { menu_number: 14, name: 'Tasty Cakes', price: 4.59, qty: 500 }
        ];
        await inventory.insertMany(mockInvetories);
        await fetch(url, { method: 'GET' })
            .then(res => res.json())
            .then(json => expect(json.length).toBeGreaterThanOrEqual(3));
    });

    it ('should update a field of an inventory document', async () => {
        const inventoryUpdate = { name: 'Tasty Cakes', menu_number: 15, price: 6.23, qty: 100 };
        const mockUrl = url + '/' + mockInventory._id;
        const options = {
            method: 'PUT',
            body: JSON.stringify(inventoryUpdate),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(mockUrl, options)
            .then(res => res.json())
            .then(json => {
                expect(json.result.ok).toBe(1);
                expect(json.document.name).toEqual(inventoryUpdate.name);
                expect(json.document.menu_number).toEqual(inventoryUpdate.menu_number);
                expect(json.document.price).toEqual(inventoryUpdate.price);
                expect(json.document.qty).toEqual(inventoryUpdate.qty);
            });
    });

    it ('should delete an inventory document', async () => {
        const mockUrl = url + '/' + mockInventory._id;
        await fetch(mockUrl, { method: 'DELETE' })
            .then(res => res.json())
            .then(json => expect(json.ok).toBe(1));
    });
});