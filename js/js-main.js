// get variables for admin tools
let menu_modal = document.getElementById("menu_modal");
let employee_modal = document.getElementById("employee_modal");
let menu_btn = document.getElementById("manage_menu_btn");
let employee_btn = document.getElementById("manage_employee_btn");
let span = document.getElementsByClassName("close")[0];

// creating static objects to test with
let burger_fries = new foodItem("N.1", "Burger with Fries", 6.99);
let wing_special = new foodItem("N.2", "The Wing Special", 7.99);
let vegan_wings = new foodItem("N.3", "Vegan Wings", 11.99);
let salad = new foodItem("N.4", "Salad", 4.99);

let charles = new newEmployee("charles", "busboy", 14.99);

// public arrays
let food_array = [burger_fries, wing_special, vegan_wings, salad];
let employee_array = [charles, charles, charles];


// creating the food object
function foodItem(item_number, item_name, item_price) {
    this.number = item_number;
    this.name = item_name;
    this.price = item_price;
}

// creating the employee object
function newEmployee(emp_name, emp_position, emp_rate){
    this.name = emp_name;
    this.position = emp_position;
    this.rate = emp_rate;
}

// manually through inputs
function createNewItem_m() {
    // getting required info
    var item_number = prompt("Enter the item number: ");
    var item_name = prompt("Enter the name of this item: ");
    var item_price = prompt("Enter how much this item costs: ");
    // creating new Object
    var newFood = new foodItem(item_number, item_name, item_price);
    food_array.push(newFood);
    alert(food_array.slice(-1)[0].name + " was added to your menu's List");
}

// automatically from db
function createNewItem_a(name, number, price) {
    // do something with the table here
    var newFood = new foodItem(number, name, price);
    food_array.push(newFood);
}

// manually through inputs
function createNewEmp_m() {
    // getting required info
    var emp_name = prompt("Enter the name of this employee: ");
    var emp_position = prompt("Enter the position of this employee: ");
    var emp_rate = prompt("Enter the rate of this employee: ");
    // creating new Object
    var newEmp = new newEmployee(emp_name, emp_position, emp_rate);
    employee_array.push(newEmp);
    alert(employee_array.slice(-1)[0].name + " was added to your Employee List");
}

// automatically from db
function createNewEmp_a(name, position, rate) {
    // do something with the table here
    var newEmp = new newEmployee(name, position, rate);
    employee_array.push(newEmp);
}




// -------------- handles the table creation -----------------
// generates table head
function generateTableHead(table, data){
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);
        th.appendChild(text);
        row.appendChild(th);
    }
}
// generates table contents
function generateTable(table, data){
    for (let element of data) {
        let row = table.insertRow();
        for (key in element) {
            let cell = row.insertCell();
            let text = document.createTextNode(element[key]);
            cell.setAttribute("class", "order_td");
            cell.appendChild(text);
        }
    }
}
let food_table = document.querySelector(".food_table");
let food_data = Object.keys(food_array[0]);

let employee_table = document.querySelector("#employee_table");
let employee_data = Object.keys(employee_array[0]);




// -------------- handles the modal popouts ------------------
// When the user clicks on the button, open the modal
menu_btn.onclick = function() {
    menu_modal.style.display = "block";
};
employee_btn.onclick = function() {
    employee_modal.style.display = "block";
};
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    employee_modal.style.display = "none";
    menu_modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == menu_modal) {
        menu_modal.style.display = "none";
    }
    if (event.target == employee_modal) {
        employee_modal.style.display = "none";
    }
};





// ---------------- button functions -------------------------
// order
$("#order_button").click(function(){
    $("#button_container").hide();
    $("#order_container").show().css("display", "inline-block");
});

// pay bill
$("#pay_button").click(function(){
    $("#button_container").hide();
    $("#pay_container").show().css("display", "inline-block");
});

// view order
$("#view_button").click(function(){
    $("#button_container").hide();
    $("#view_container").show().css("display", "inline-block");
});

// call waiter
$("#call_button").click(function(){
    $("#button_container").hide();
    $("#calling_container").show().css("display", "inline-block");
});

// cancel calling waiter
$("#cancel_button").click(function () {
    $("#calling_container").hide();
    $("#button_container").show().css("display", "inline-block");
});

// back to menu button
$(".return_button").click(function(){
    $("#order_container").hide();
    $("#pay_container").hide();
    $("#view_container").hide();
    $("#calling_container").hide();
    $("#tool_container").hide();
    $("#button_container").show().css("display", "inline-block");
});

// admin tools
$("#admin_functions").click(function() {
    $("#button_container").hide();
    $("#order_container").hide();
    $("#pay_container").hide();
    $("#view_container").hide();
    $("#calling_container").hide();
    $("#tool_container").show().css("display", "inline-block");
});


// admin tool manage menu
// update menu button
$("#update_btn").click(function() {
    const url = 'http://localhost:5000/menuItems';
    const othePram={
        method: "GET"
    };

    fetch(url, othePram)
        .then(data=>{return data.json()})
        .then(res=>{for (var i = 0; i < res.length; i++) {
            let name = res[i].name;
            let number = res[i].number;
            let price = res[i].price;
            createNewItem_a(name, number, price);
        }})
        .catch(error=>console.log(error));
    alert("Menu has been updated with the latest items in the DB.");
});
// add menu item
$("#add_item_btn").click(function() {
    createNewItem_m();
});
// generates the menu
$("#generate_menu_btn").click(function() {
    // generate table
    generateTable(food_table, food_array);
    generateTableHead(food_table, food_data);
});



// update employee button
$("#update_emp_btn").click(function() {
    const url = 'http://localhost:5000/employees';
    const othePram={
        method: "GET"
    };

    fetch(url, othePram)
        .then(data=>{return data.json()})
        .then(res=>{for (var i = 0; i < res.length; i++) {
            let name = res[i].name;
            let position = res[i].position;
            let rate = res[i].rate;
            createNewEmp_a(name, position, rate);
        }})
        .catch(error=>console.log(error));
    alert("Employee's info has been updated with the latest DB.");
});
// add employee
$("#add_emp_btn").click(function() {
    createNewEmp_m();
});
// generates the employee table
$("#generate_emp_btn").click(function() {
    // generate table
    generateTable(employee_table, employee_array);
    generateTableHead(employee_table, employee_data);
});
